
# Nationella prostatacancerregistret (NPCR)

Skript för clean data, beräknade variabler, etc.

## Användning

  - Ladda hem .RData-fil från INCA, t.ex. från någon av vyerna:
      - *Prostata\_Nationell*
      - *Prostata\_Nationell\_EjID*
      - *Prostata\_Regional*
  - Ladda även hem .RData-fil från formuläret Kompletterande
    undersökning, t.ex. från någon av vyerna:
      - *Prostata\_Nationell\_KompletterandeUtredning*
      - *Prostata\_Nationell\_EjID\_KompletterandeUtredning*
      - *Prostata\_Regional\_KompletterandeUtredning*
  - Kör **main.R** för clean data.
  - Se separat repo **npcr-rccShiny-ind** för definition av bl.a.
    kvalitetsindikatorer.

## Resultat

En `tbl_df` med beräknade variabler, t.ex.:

    primary_treatment
    primary_treatment_detailed
    d0_ ...
    b0_ ...
    rp0_ ...
    s0_ ...
