incaCleanNumeric <- 
  function(x) {
    x <- as.character(x)
    x[grepl("[[:alpha:]]", x)] <- ""
    x[grepl("[^[:alnum:]]", x)] <- ""
    x <- as.numeric(x)
    x
  }