rowMax <- 
  function(x) {
    apply(
      X = x, 
      MARGIN = 1, 
      FUN = function(x) {
        ifelse(
          all(is.na(x)), 
          NA, 
          max(x, na.rm = TRUE)
        )
      }
    )
  }
npcrPrepareGleason <- 
  function(
    gleason1 = NULL,
    gleason2 = NULL,
    gleasonsum = NULL
  ) {
    if (!(all(dim(gleason1) == dim(gleason2)) & all(dim(gleason1) == dim(gleasonsum)))) {
      stop("Dimensions of 'gleason1', 'gleason2' and 'gleasonsum' differ", call. = FALSE)
    }
    
    # Manuella rättningar
    tempSelection <- is.na(gleasonsum) & !is.na(gleason1) & !is.na(gleason2)
    gleasonsum[tempSelection] <- gleason1[tempSelection] + gleason2[tempSelection]
    tempSelection <- !is.na(gleasonsum) & is.na(gleason1) & !is.na(gleason2)
    gleason1[tempSelection] <- gleasonsum[tempSelection] - gleason2[tempSelection]
    tempSelection <- !is.na(gleasonsum) & !is.na(gleason1) & is.na(gleason2)
    gleason2[tempSelection] <- gleasonsum[tempSelection] - gleason1[tempSelection]
    
    # Bestäm i vilken/vilka kolumn(er) max Gleasonsumma finns
    gleasonsumMax <- 
      matrix(
        data = rep(rowMax(gleasonsum), ncol(gleasonsum)),
        ncol = ncol(gleasonsum),
        byrow = FALSE
      )
    gleasonsumKeep <- !is.na(gleasonsum) & gleasonsum == gleasonsumMax
    gleasonsum[!gleasonsumKeep] <- NA
    gleason1[!gleasonsumKeep] <- NA
    gleason2[!gleasonsumKeep] <- NA
    
    # Bestäm i vilken/vilka kolumn(er) max Gleason 1 finns (av de där Gleasonsumma är max)
    gleason1Max <- 
      matrix(
        data = rep(rowMax(gleason1), ncol(gleason1)),
        ncol = ncol(gleason1),
        byrow = FALSE
      )
    gleason1Keep <- !is.na(gleason1) & gleason1 == gleason1Max
    gleason1[!gleason1Keep] <- NA
    gleason2[!gleason1Keep] <- NA
    
    data.frame(
      gleason1 = rowMax(gleason1),
      gleason2 = rowMax(gleason2),
      gleasonsum = rowMax(gleasonsum),
      stringsAsFactors = FALSE
    )
  }