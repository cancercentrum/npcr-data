npcrCalculateRiskgroup <- 
  function(
    data,
    at = "d0",
    returnDetailed = FALSE,
    returnDataWithAllVariables = FALSE
  ) {
    variablesRequired <- c(
      "d_diadat",
      "rp_opanmdat",
      "s_stralanmdat",
      "d_tstad",
      "d_nstad",
      "d_mstad",
      "b_bildlymfnstadium",
      "b_bildskelmstadium",
      "rp_bildlymfnstadium",
      "rp_bildskelmstadium",
      "s_diagbildlymf",
      "s_diagbildlymfny",
      "s_diagbildfjarr",
      "d_spsa",
      "d_gleasett",
      "d_gleastva",
      "d_gleassa",
      "d_diffgrad",
      "d_biop",
      "d_biopca",
      "d_mmcancer",
      "d_vol",
      "kom0_d0_t",
      "kom0_d0_n",
      "kom0_d0_m",
      "kom0_d0_vol",
      "kom0_d0_psa",
      "kom0_d0_biop",
      "kom0_d0_biopca",
      "kom0_d0_mmcancer",
      "kom0_d0_gleasett",
      "kom0_d0_gleastva",
      "kom0_d0_gleassa",
      "kom0_d0_mr",
      "d_epe",
      "d_svi"
    )
    variablesMissing <- variablesRequired[!(variablesRequired) %in% colnames(data)]
    if (length(variablesMissing) > 0) {
      stop(
        paste0(
          "Missing required variables in data: ",
          paste(
            variablesMissing,
            collapse = ", "
          )
        ),
        call. = FALSE
      )
    }
    
    factorLevels <- function(detailed = FALSE, version = "") {
      if (detailed) {
        if (version == "2025") {
          # 2025-02-21: Beslut i styrgruppen: Dela inte upp i mycket låg risk/övrig lågrisk
          # c(11:13, 21:22, 31:32, 4:6)
          c(1, 21:22, 31:32, 4:6)
        } else {
          c(11:13, 2, 31:32, 4:6)
        }
      } else {
        1:6
      }
    }
    factorLabels <- 
      function(detailed = FALSE, version = "") {
        if (version == "2025") {
          if (detailed) {
            # 2025-02-21: Beslut i styrgruppen: Dela inte upp i mycket låg risk/övrig lågrisk
            # c("Mycket låg risk", "Lågrisk (övrig)", "Lågrisk (saknas)", "Gynnsam mellanrisk", "Ogynnsam mellanrisk", "Högrisk", "Mycket hög risk", "Regionalt metastaserad", "Fjärrmetastaserad", "Uppgift saknas")
            c("Lågrisk", "Gynnsam mellanrisk", "Ogynnsam mellanrisk", "Högrisk", "Mycket hög risk", "Regionalt metastaserad", "Fjärrmetastaserad", "Uppgift saknas")
          } else {
            c("Lågrisk", "Mellanrisk", "Högrisk + Mycket hög risk", "Regionalt metastaserad", "Fjärrmetastaserad", "Uppgift saknas")
          }
        } else {
          if (detailed) {
            c("Mycket låg risk", "Lågrisk (övrig)", "Lågrisk (saknas)", "Mellanrisk", "Lokaliserad högrisk", "Lokalt avancerad", "Regionalt metastaserad", "Fjärrmetastaserad", "Uppgift saknas")
          } else {
            c("Lågrisk", "Mellanrisk", "Högrisk", "Regionalt metastaserad", "Fjärrmetastaserad", "Uppgift saknas")
          }
        }
      }
    
    if ("primary_treatment" %in% colnames(data)) {
      data$tempPrimaryTreatment <- data$primary_treatment
    } else {
      data$tempPrimaryTreatment <- "99x missing"
    }
    
    # Riskgrupp vid diagnos --------------------------------------------------------
    data <- data %>% 
      
      dplyr::mutate(
        
        # T
        t = dplyr::if_else(
          !is.na(kom0_d0_t),
          as.numeric(kom0_d0_t),
          as.numeric(d_tstad)
        ),
        
        # N
        getImagingFromRP = !is.na(d_diadat) & !is.na(rp_opanmdat) & (rp_opanmdat - d_diadat) <= 365.24,
        getImagingFromS = !is.na(d_diadat) & !is.na(s_stralanmdat) & (s_stralanmdat - d_diadat) <= 365.24,
        s_diagbildlymf = dplyr::if_else(
          !(s_diagbildlymf %in% 0:1),
          as.numeric(s_diagbildlymfny),
          as.numeric(s_diagbildlymf)
        ),
        n = dplyr::case_when(
          kom0_d0_n %in% 1 | b_bildlymfnstadium %in% 1 | (getImagingFromRP & rp_bildlymfnstadium %in% 1) | (getImagingFromS & s_diagbildlymf %in% 1) ~ 1,
          kom0_d0_n %in% 0 | b_bildlymfnstadium %in% 0 | (getImagingFromRP & rp_bildlymfnstadium %in% 0) | (getImagingFromS & s_diagbildlymf %in% 0) ~ 0,
          d_nstad %in% 1 ~ 1,
          d_nstad %in% 0 ~ 0,
          b_bildlymfnstadium %in% c(9, 97) | (getImagingFromRP & rp_bildlymfnstadium %in% c(9, 97)) | (getImagingFromS & s_diagbildlymf %in% c(9, 97)) ~ 9,
          d_nstad %in% c(9, 97) ~ 9,
          TRUE ~ as.numeric(NA)
        ),
        
        # M
        m = dplyr::case_when(
          kom0_d0_m %in% 1 | b_bildskelmstadium %in% 1 | (getImagingFromRP & rp_bildskelmstadium %in% 1) | (getImagingFromS & s_diagbildfjarr %in% 1) ~ 1,
          kom0_d0_m %in% 0 | b_bildskelmstadium %in% 0 | (getImagingFromRP & rp_bildskelmstadium %in% 0) | (getImagingFromS & s_diagbildfjarr %in% 0) ~ 0,
          d_mstad %in% 1 ~ 1,
          d_mstad %in% 0 ~ 0,
          b_bildskelmstadium %in% c(9, 97) | (getImagingFromRP & rp_bildskelmstadium %in% c(9, 97)) | (getImagingFromS & s_diagbildfjarr %in% c(9, 97)) ~ 9,
          d_mstad %in% c(9, 97) ~ 9
        ),
        
        # PSA
        psa = dplyr::if_else(
          !is.na(kom0_d0_psa),
          as.numeric(kom0_d0_psa),
          as.numeric(d_spsa)
        ),
        
        # Gleasonsumma (för vissa äldre data har Gleasonsumman inte räknats ut automatiskt från Gleason 1 och Gleason 2)
        gleassa = dplyr::case_when(
          !is.na(kom0_d0_gleassa) ~ as.numeric(kom0_d0_gleassa),
          is.na(d_gleassa) & d_gleasett %in% 1:5 & d_gleastva %in% 1:5 ~ as.numeric(d_gleasett + d_gleastva),
          TRUE ~ as.numeric(d_gleassa)
        ),
        gleasett = dplyr::if_else(
          !is.na(kom0_d0_gleassa),
          as.numeric(kom0_d0_gleasett),
          as.numeric(d_gleasett)
        ),
        gleastva = dplyr::if_else(
          !is.na(kom0_d0_gleassa),
          as.numeric(kom0_d0_gleastva),
          as.numeric(d_gleastva)
        ),
        
        # Differentieringsgrad enligt WHO
        diffgrad = d_diffgrad,
        
        # Biopsier
        biop = dplyr::if_else(
          !is.na(kom0_d0_biop),
          as.numeric(kom0_d0_biop),
          as.numeric(d_biop)
        ),
        biopca = dplyr::if_else(
          !is.na(kom0_d0_biopca),
          as.numeric(kom0_d0_biopca),
          as.numeric(d_biopca)
        ),
        mmcancer = dplyr::if_else(
          !is.na(kom0_d0_mmcancer),
          as.numeric(kom0_d0_mmcancer),
          as.numeric(d_mmcancer)
        ),
        
        # Prostatavolym
        vol = dplyr::case_when(
          !is.na(kom0_d0_vol) & kom0_d0_vol > 0 ~ as.numeric(kom0_d0_vol),
          !is.na(d_vol) & d_vol > 0 ~ as.numeric(d_vol),
          TRUE ~ as.numeric(NA),
        ),
        
        # EPE/SVI
        epe = dplyr::case_when(
          !is.na(kom0_d0_mr) & !(kom0_d0_mr %in% 0) ~ as.numeric(NA),
          TRUE ~ as.numeric(d_epe),
        ),
        svi = dplyr::case_when(
          !is.na(kom0_d0_mr) & !(kom0_d0_mr %in% 0) ~ as.numeric(NA),
          TRUE ~ as.numeric(d_svi),
        )
        
      )
    
    # Riskgrupp vid RP -------------------------------------------------------------
    if (at == "rp0") {
      variablesRequired <- c(
        "rp_nyundersoknutf",
        "rp_nybedtstad",
        "rp_bildlymf",
        "rp_bildskelett",
        "rp_nypsapreop",
        "rp_bioputf",
        "rp_trulutf",
        "rp_tstad",
        "rp_bildlymfnstadium",
        "rp_bildskelmstadium",
        "rp_psa",
        "rp_biopgleassa",
        "rp_biopgleasett",
        "rp_biopgleastva",
        "rp_biop",
        "rp_biopca",
        "rp_mmcancer",
        "rp_vol",
        "kom0_amavslut0_t",
        "kom0_amavslut0_n",
        "kom0_amavslut0_m",
        "kom0_amavslut0_vol",
        "kom0_amavslut0_psa",
        "kom0_amavslut0_biop",
        "kom0_amavslut0_biopca",
        "kom0_amavslut0_mmcancer",
        "kom0_amavslut0_gleasett",
        "kom0_amavslut0_gleastva",
        "kom0_amavslut0_gleassa",
        "kom0_amavslut0_mr"
      )
      variablesMissing <- variablesRequired[!(variablesRequired) %in% colnames(data)]
      if (length(variablesMissing) > 0) {
        stop(
          paste0(
            "Missing required variables in data: ",
            paste(
              variablesMissing,
              collapse = ", "
            )
          ),
          call. = FALSE
        )
      }
      
      data <- data %>% 
        
        dplyr::mutate(
          
          # T
          t = dplyr::case_when(
            !is.na(rp_tstad) ~ as.numeric(rp_tstad),
            !is.na(kom0_amavslut0_t) ~ as.numeric(kom0_amavslut0_t),
            rp_nyundersoknutf %in% 0 | rp_nybedtstad %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(t),
            TRUE ~ as.numeric(NA)
          ),
          
          # N
          n = dplyr::case_when(
            rp_bildlymfnstadium %in% 1 | kom0_amavslut0_n %in% 1 ~ as.numeric(1),
            rp_bildlymfnstadium %in% 0 | kom0_amavslut0_n %in% 0 ~ as.numeric(0),
            rp_bildlymf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(n),
            TRUE ~ as.numeric(NA)
          ),
          
          # M
          m = dplyr::case_when(
            rp_bildskelmstadium %in% 1 | kom0_amavslut0_m %in% 1 ~ as.numeric(1),
            rp_bildskelmstadium %in% 0 | kom0_amavslut0_m %in% 0 ~ as.numeric(0),
            rp_bildskelett %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(m),
            TRUE ~ as.numeric(NA)
          ),
          
          # PSA
          psa = dplyr::case_when(
            !is.na(rp_psa) ~ as.numeric(rp_psa),
            !is.na(kom0_amavslut0_psa) ~ as.numeric(kom0_amavslut0_psa),
            rp_nyundersoknutf %in% 0 | rp_nypsapreop %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(psa),
            TRUE ~ as.numeric(NA)
          ),
          
          # Gleasonsumma
          gleassa = dplyr::case_when(
            !is.na(rp_biopgleassa) ~ as.numeric(rp_biopgleassa),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleassa),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(gleassa),
            TRUE ~ as.numeric(NA)
          ),
          gleasett = dplyr::case_when(
            !is.na(rp_biopgleassa) ~ as.numeric(rp_biopgleasett),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleasett),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(gleasett),
            TRUE ~ as.numeric(NA)
          ),
          gleastva = dplyr::case_when(
            !is.na(rp_biopgleassa) ~ as.numeric(rp_biopgleastva),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleastva),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(gleastva),
            TRUE ~ as.numeric(NA)
          ),
          
          # Differentieringsgrad enligt WHO
          diffgrad = dplyr::case_when(
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(diffgrad),
            TRUE ~ as.numeric(NA)
          ),
          
          # Biopsier
          biop = dplyr::case_when(
            !is.na(rp_biop) ~ as.numeric(rp_biop),
            !is.na(kom0_amavslut0_biop) ~ as.numeric(kom0_amavslut0_biop),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(biop),
            TRUE ~ as.numeric(NA)
          ),
          biopca = dplyr::case_when(
            !is.na(rp_biopca) ~ as.numeric(rp_biopca),
            !is.na(kom0_amavslut0_biopca) ~ as.numeric(kom0_amavslut0_biopca),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(biopca),
            TRUE ~ as.numeric(NA)
          ),
          mmcancer = dplyr::case_when(
            !is.na(rp_mmcancer) ~ as.numeric(rp_mmcancer),
            !is.na(kom0_amavslut0_mmcancer) ~ as.numeric(kom0_amavslut0_mmcancer),
            rp_nyundersoknutf %in% 0 | rp_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(mmcancer),
            TRUE ~ as.numeric(NA)
          ),
          
          # Prostatavolym
          vol = dplyr::case_when(
            !is.na(rp_vol) ~ as.numeric(rp_vol),
            !is.na(kom0_amavslut0_vol) ~ as.numeric(kom0_amavslut0_vol),
            rp_nyundersoknutf %in% 0 | rp_trulutf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(vol),
            TRUE ~ as.numeric(NA)
          ),
          
          # EPE/SVI
          epe = dplyr::case_when(
            !is.na(kom0_amavslut0_mr) & !(kom0_amavslut0_mr %in% 0) ~ as.numeric(NA),
            substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(epe),
            TRUE ~ as.numeric(NA),
          ),
          svi = dplyr::case_when(
            !is.na(kom0_amavslut0_mr) & !(kom0_amavslut0_mr %in% 0) ~ as.numeric(NA),
            substr(tempPrimaryTreatment, 1, 2) %in% "21" ~ as.numeric(svi),
            TRUE ~ as.numeric(NA),
          )
          
        )
    }
    
    # Riskgrupp vid RT -------------------------------------------------------------
    if (at == "s0") {
      variablesRequired <- c(
        "s_nyundersoknutf",
        "s_nybedtstad",
        "s_bildlymf",
        "s_bildskelett",
        "s_nypsaprert",
        "s_bioputf",
        "s_tstad",
        "s_diagbildlymf",
        "s_diagbildfjarr",
        "s_spsa",
        "s_biopgleassa",
        "s_biopgleasett",
        "s_biopgleastva",
        "s_biop",
        "s_biopca",
        "s_mmcancer",
        "s0_postop",
        "kom0_amavslut0_t",
        "kom0_amavslut0_n",
        "kom0_amavslut0_m",
        "kom0_amavslut0_vol",
        "kom0_amavslut0_psa",
        "kom0_amavslut0_biop",
        "kom0_amavslut0_biopca",
        "kom0_amavslut0_mmcancer",
        "kom0_amavslut0_gleasett",
        "kom0_amavslut0_gleastva",
        "kom0_amavslut0_gleassa",
        "kom0_amavslut0_mr"
      )
      variablesMissing <- variablesRequired[!(variablesRequired) %in% colnames(data)]
      if (length(variablesMissing) > 0) {
        stop(
          paste0(
            "Missing required variables in data: ",
            paste(
              variablesMissing,
              collapse = ", "
            )
          ),
          call. = FALSE
        )
      }
      
      data <- data %>% 
        
        dplyr::mutate(
          
          # T
          t = dplyr::case_when(
            !is.na(s_tstad) ~ as.numeric(s_tstad),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_t) ~ as.numeric(kom0_amavslut0_t),
            s_nyundersoknutf %in% 0 | s_nybedtstad %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(t),
            TRUE ~ as.numeric(NA)
          ),
          
          # N
          n = dplyr::case_when(
            s0_postop %in% 1 & s_diagbildlymf %in% 1 ~ as.numeric(1),
            s0_postop %in% 1 & s_diagbildlymf %in% 0 ~ as.numeric(0),
            s0_postop %in% 1 ~ as.numeric(NA),
            s_diagbildlymf %in% 1 | kom0_amavslut0_n %in% 1 ~ as.numeric(1),
            s_diagbildlymf %in% 0 | kom0_amavslut0_n %in% 0 ~ as.numeric(0),
            s_bildlymf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(n),
            TRUE ~ as.numeric(NA)
          ),
          
          # M
          m = dplyr::case_when(
            s0_postop %in% 1 & s_diagbildfjarr %in% 1 ~ as.numeric(1),
            s0_postop %in% 1 & s_diagbildfjarr %in% 0 ~ as.numeric(0),
            s0_postop %in% 1 ~ as.numeric(NA),
            s_diagbildfjarr %in% 1 | kom0_amavslut0_m %in% 1 ~ as.numeric(1),
            s_diagbildfjarr %in% 0 | kom0_amavslut0_m %in% 0 ~ as.numeric(0),
            s_bildskelett %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(m),
            TRUE ~ as.numeric(NA)
          ),
          
          # PSA
          psa = dplyr::case_when(
            !is.na(s_spsa) ~ as.numeric(s_spsa),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_psa) ~ as.numeric(kom0_amavslut0_psa),
            s_nyundersoknutf %in% 0 | s_nypsaprert %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(psa),
            TRUE ~ as.numeric(NA)
          ),
          
          # Gleasonsumma
          gleassa = dplyr::case_when(
            !is.na(s_biopgleassa) ~ as.numeric(s_biopgleassa),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleassa),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(gleassa),
            TRUE ~ as.numeric(NA)
          ),
          gleasett = dplyr::case_when(
            !is.na(s_biopgleassa) ~ as.numeric(s_biopgleasett),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleasett),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(gleasett),
            TRUE ~ as.numeric(NA)
          ),
          gleastva = dplyr::case_when(
            !is.na(s_biopgleassa) ~ as.numeric(s_biopgleastva),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_gleassa) ~ as.numeric(kom0_amavslut0_gleastva),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(gleastva),
            TRUE ~ as.numeric(NA)
          ),
          
          # Differentieringsgrad enligt WHO
          diffgrad = dplyr::case_when(
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(diffgrad),
            TRUE ~ as.numeric(NA)
          ),
          
          # Biopsier
          biop = dplyr::case_when(
            !is.na(s_biop) ~ as.numeric(s_biop),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_biop) ~ as.numeric(kom0_amavslut0_biop),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(biop),
            TRUE ~ as.numeric(NA)
          ),
          biopca = dplyr::case_when(
            !is.na(s_biopca) ~ as.numeric(s_biopca),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_biopca) ~ as.numeric(kom0_amavslut0_biopca),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(biopca),
            TRUE ~ as.numeric(NA)
          ),
          mmcancer = dplyr::case_when(
            !is.na(s_mmcancer) ~ as.numeric(s_mmcancer),
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_mmcancer) ~ as.numeric(kom0_amavslut0_mmcancer),
            s_nyundersoknutf %in% 0 | s_bioputf %in% 0 | substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(mmcancer),
            TRUE ~ as.numeric(NA)
          ),
          
          # Prostatavolym
          vol = dplyr::case_when(
            s0_postop %in% 1 ~ as.numeric(NA),
            !is.na(kom0_amavslut0_vol) ~ as.numeric(kom0_amavslut0_vol),
            TRUE ~ as.numeric(NA)
          ),
          
          # EPE/SVI
          epe = dplyr::case_when(
            !is.na(kom0_amavslut0_mr) & !(kom0_amavslut0_mr %in% 0) ~ as.numeric(NA),
            substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(epe),
            TRUE ~ as.numeric(NA),
          ),
          svi = dplyr::case_when(
            !is.na(kom0_amavslut0_mr) & !(kom0_amavslut0_mr %in% 0) ~ as.numeric(NA),
            substr(tempPrimaryTreatment, 1, 2) %in% "22" ~ as.numeric(svi),
            TRUE ~ as.numeric(NA),
          )
          
        )
    }
    
    # Uträkning av riskgrupp -------------------------------------------------------
    data <- data %>% 
      dplyr::mutate(
        # Gamla versionen
        riskgroup_old = dplyr::case_when(
          (!is.na(psa) & psa >= 100) | m %in% 1 ~ 5,
          (!is.na(psa) & psa >= 50) | n %in% 1 | t %in% 4 ~ 4,
          t %in% 3 | gleassa %in% 8:10 | (is.na(gleassa) & diffgrad %in% 3) | (!is.na(psa) & psa >= 20) ~ 3,
          t %in% c(0, 1, 11:13, 2) & (gleassa %in% 7 | (is.na(gleassa) & diffgrad %in% 2) | (!is.na(psa) & psa >= 10)) ~ 2,
          t %in% c(0, 1, 11:13, 2) & (gleassa %in% 2:6 | (is.na(gleassa) & diffgrad %in% 1)) & (!is.na(psa) & psa < 10) ~ 1,
          TRUE ~ 6
        ),
        riskgroup_detailed_old = dplyr::case_when(
          riskgroup_old %in% 3 & t %in% 3 ~ 32,
          riskgroup_old %in% 3 ~ 31,
          riskgroup_old %in% 1 & 
            t %in% 13 & 
            !is.na(biopca) & biopca <= 4 & 
            !is.na(biop) & biop >= 8 & 
            !is.na(mmcancer) & mmcancer < 8 & 
            !is.na(psa/vol) & (psa/vol) < 0.15 ~ 11,
          riskgroup_old %in% 1 & (
            (!is.na(t) & !(t %in% 13)) | 
              (!is.na(biopca) & biopca > 4) | 
              (!is.na(biop) & biop < 8) | 
              (!is.na(mmcancer) & mmcancer >= 8) | 
              !is.na(psa/vol) & psa/vol >= 0.15
          ) ~ 12,
          riskgroup_old %in% 1 ~ 13,
          TRUE ~ riskgroup_old
        ),
        riskgroup_old = factor(
          riskgroup_old,
          levels = factorLevels(),
          labels = factorLabels()
        ),
        riskgroup_detailed_old = factor(
          riskgroup_detailed_old,
          levels = factorLevels(detailed = TRUE),
          labels = factorLabels(detailed = TRUE)
        ),
        # Nya versionen
        riskgroup = dplyr::case_when(
          m %in% 1 ~ 5,
          n %in% 1 ~ 4,
          t %in% 3:4 | epe %in% 5 | svi %in% 5 | gleassa %in% 8:10 | (is.na(gleassa) & diffgrad %in% 3) | (!is.na(psa) & psa >= 20) ~ 3,
          t %in% 2 | gleassa %in% 7 | (is.na(gleassa) & diffgrad %in% 2) | (!is.na(psa) & psa >= 10) ~ 2,
          t %in% c(0, 1, 11:13) & (gleassa %in% 2:6 | (is.na(gleassa) & diffgrad %in% 1)) & (!is.na(psa) & psa < 10) ~ 1,
          TRUE ~ 6
        ),
        temp_highrisk1_t = 1 * (t %in% 3) | epe %in% 5 | svi %in% 5,
        temp_highrisk1_gleassa = 1 * (gleassa %in% 8:10 | (is.na(gleassa) & diffgrad %in% 3)),
        temp_highrisk1_psa = 1 * (!is.na(psa) & psa >= 20),
        temp_intermediaterisk1_t = 1 * (t %in% 2),
        temp_intermediaterisk1_gleassa = 1 * (gleassa %in% 7 | (is.na(gleassa) & diffgrad %in% 2)),
        temp_intermediaterisk1_psa = 1 * (!is.na(psa) & psa >= 10),
        riskgroup_detailed = dplyr::case_when(
          riskgroup %in% 3 & (t %in% 4 | gleassa %in% 9:10 | (!is.na(psa) & psa >= 40)) ~ 32,
          riskgroup %in% 3 & (temp_highrisk1_t + temp_highrisk1_gleassa + temp_highrisk1_psa) >= 2 ~ 32,
          riskgroup %in% 3 ~ 31,
          riskgroup %in% 2 & gleasett %in% 4 & gleastva %in% 3 ~ 22,
          riskgroup %in% 2 & (temp_intermediaterisk1_t + temp_intermediaterisk1_gleassa + temp_intermediaterisk1_psa) >= 2 ~ 22,
          riskgroup %in% 2 ~ 21,
          # 2025-02-21: Beslut i styrgruppen: Dela inte upp i mycket låg risk/övrig lågrisk
          # riskgroup %in% 1 & 
          #   t %in% 13 & 
          #   !is.na(biopca) & biopca <= 4 & 
          #   !is.na(biop) & biop >= 8 & 
          #   !is.na(mmcancer) & mmcancer <= 8 & 
          #   !is.na(psa/vol) & (psa/vol) < 0.15 ~ 11,
          # riskgroup %in% 1 & (
          #   (!is.na(t) & !(t %in% 13)) | 
          #     (!is.na(biopca) & biopca > 4) | 
          #     (!is.na(biop) & biop < 8) | 
          #     (!is.na(mmcancer) & mmcancer >= 8) | 
          #     !is.na(psa/vol) & psa/vol >= 0.15
          # ) ~ 12,
          # riskgroup %in% 1 ~ 13,
          TRUE ~ riskgroup
        ),
        riskgroup = factor(
          riskgroup,
          levels = factorLevels(version = "2025"),
          labels = factorLabels(version = "2025")
        ),
        riskgroup_detailed = factor(
          riskgroup_detailed,
          levels = factorLevels(detailed = TRUE, version = "2025"),
          labels = factorLabels(detailed = TRUE, version = "2025")
        ),
      ) %>% 
        dplyr::select(
          -dplyr::starts_with("temp_highrisk1_"),
          -dplyr::starts_with("temp_intermediaterisk1_")
        )
    
    if (returnDataWithAllVariables) {
      data <- data %>% 
        dplyr::select(
          -tempPrimaryTreatment,
          -getImagingFromRP,
          -getImagingFromS
        ) %>%
        dplyr::rename(
          !!paste0(at, "_t") := t,
          !!paste0(at, "_n") := n,
          !!paste0(at, "_m") := m,
          !!paste0(at, "_psa") := psa,
          !!paste0(at, "_gleassa") := gleassa,
          !!paste0(at, "_gleasett") := gleasett,
          !!paste0(at, "_gleastva") := gleastva,
          !!paste0(at, "_diffgrad") := diffgrad,
          !!paste0(at, "_biop") := biop,
          !!paste0(at, "_biopca") := biopca,
          !!paste0(at, "_mmcancer") := mmcancer,
          !!paste0(at, "_vol") := vol,
          !!paste0(at, "_riskgroup_old") := riskgroup_old,
          !!paste0(at, "_riskgroup_detailed_old") := riskgroup_detailed_old,
          !!paste0(at, "_riskgroup") := riskgroup,
          !!paste0(at, "_riskgroup_detailed") := riskgroup_detailed
        )
      data
    } else {
      if (returnDetailed) {
        data$riskgroup_detailed
      } else {
        data$riskgroup
      }
    }
  }